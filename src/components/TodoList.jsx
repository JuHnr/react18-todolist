import PropTypes from "prop-types";
import TodoItem from "./TodoItem"; //imporation du composant pour l'utiliser dans .map

function TodoList(props) {
  const { todos } = props;

  // Création d'une liste de composants TodoItem en utilisant la méthode .map
  
  //on crée un nouveau tableau listItems qui contiendra le nouveau tableau renvoyé par la méthode .map
  //on applique la méthode .map au tableau de données todos (initialisé dans App.jsx, passé en paramètre de TodoList)
  //la méthode .map itère dans le tableau et effectue une transformation pour chacun des éléments:
  //ici, chaque élément est passé en tant que props du composant TodoItem 
  //TodoItem s'occupe de créer une <li> pour l'élément (voir le composant)
  //pour finir, on ajoute la clé unique de l'élément (item.id) pour qu'il soit identifiable peu importe les changements apportés au tableau (tri, suppression..)
  const listItems = todos.map(item => <TodoItem key={item.id} {...item} />);

  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>

      {/* Affiche le tableau de composants listItems créé par .map */}
      <ul>{listItems}</ul>

    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
